# `Plymouth` sweet-arch theme

`Plymouth` sweet-arch theme by
[abrahammurciano](https://github.com/abrahammurciano);

## Install

Copy [sweet-arch](sweet-arch) into `/usr/share/plymouth/themes` and write this:

```conf
[Daemon]
Theme=sweet-arch
```

inside `/etc/plymouth/plymouthd.conf`.
